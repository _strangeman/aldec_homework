#include <stdio.h>

double findVectorCoordinate(double vectorStart, double vectorEnd)
{
	return (vectorEnd - vectorStart);
}

/*
// Функция сработает неверно, если хоть один из направляющих векторов имеет нулевую координату
bool isParallel(double xVector1, double yVector1, double xVector2, double yVector2)
{
	return ((xVector1 / xVector2) == (yVector1 / yVector2));
}
*/

bool isParallel(double xVector1, double yVector1, double xVector2, double yVector2)
{
	//Вычисляем скалярное произведение векторов и произведение их длин. Если они равны, то косинус угла между векторами (прямыми) равен 1 и два вектора паралельны
	return (((xVector1*xVector2+yVector1*yVector2)*(xVector1*xVector2+yVector1*yVector2)) ==
			((xVector1*xVector1+yVector1*yVector1)*(xVector2*xVector2+yVector2*yVector2)));
}

int main()
{
	double x1, y1, x2, y2, x3, y3, x4, y4, n1, m1, n2, m2;
	printf("Input coordinates of 4 points in format:\nx1 y1 x2 y2 x3 y3 x4 y4: ");
	scanf("%lf %lf %lf %lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &x3, &y3, &x4, &y4);
	n1 = findVectorCoordinate(x1, x2);
	m1 = findVectorCoordinate(y1, y2);
	n2 = findVectorCoordinate(x3, x4);
	m2 = findVectorCoordinate(y3, y4);
	if (isParallel(n1, m1, n2, m2))
	{
		printf("YES, straights are parallel");
	}
	else {
		printf("NO, straights are not parallel");
	}
	return 0;
}
