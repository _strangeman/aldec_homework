#include <stdio.h>

int main() {
	int amountOfMemory;
	printf("Input amount of memory in Mb's: ");
	scanf("%d", &amountOfMemory);
	if (amountOfMemory < (4*1024)) {
		printf("32");
	}
	else {
		printf("Only 64");
	}
	printf("-bit computer can address %d Mb of memory", amountOfMemory);
	return 0;
}
